import os
import json
import socket
from machine import Pin, PWM

flash = PWM(Pin(2),freq=1)

pins = [Pin(x,Pin.IN|Pin.PULL_UP) for x in [14,12,13,15,4,5]]


def load_config():
    config = {
       'host' : 'robot.local',
       'port' : 20807
    }
    with open('control.json') as fp:
        config.update(json.load(fp))
    return config


def init_irq():
    irqs=[]
    for pin in pins:
        # don't know why this has to be done
        pin.on()
        irqs.append(pin.irq(lambda p: send (s, p)))
    return irqs


def connect(config):
    flash.duty(512)
    s = socket.socket()
    host = socket.getaddrinfo(config['host'],config['port'])[0][-1]
    s.connect(host)
    flash.duty(0)
    return s


def send(s, pin):
    try:
        val=f'{pins.index(pin)}:{pin.value()}\n'
        s.send(val.encode())
    except:
        os.reset()


def main():
    irqs = init_irq()
    config = load_config()
    s = connect(config)

    while True:
        pass


if __name__ == '__main__':
    main()
