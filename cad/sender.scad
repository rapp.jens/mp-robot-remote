use <electrics/d1mini.scad>

module mount(d=3,h=10) {
	translate([-52,0])cylinder(d=d,h=h,center=true);
	translate([ 52,0])cylinder(d=d,h=h,center=true);
}

module baseshape(d=0,h=2) {
	d2=d*2;
	hull() {
		translate([-30,0])cylinder(d=50+d2,h=h,center=true);
		translate([ 30,0])cylinder(d=50+d2,h=h,center=true);
		translate([0,-25.5-d])cube([24+d2,10,h],center=true);
	}

}
module front() {
	difference() {
		union() {
			translate([0,-11,3.5])d1mini_bracket();
			baseshape();
		}
		translate([0,-30])cube([14,10,2],center=true);
		translate([-11.5,-12])cube([3,23,3],center=true);
		translate([ 11.5,-12])cube([3,23,3],center=true);
		mount($fn=16);
		
		//switches
		// left 
		translate([32,2]) {
			for (r = [1:4]) {
				rotate([0,0, 90 * r])translate([0,-10])
					cylinder(d=7.5,h=10,center=true);
			}
		}
		// right
		translate([-32,2]) {
			for (r = [1:4]) {
				rotate([0,0, 90 * r])translate([0,-10])
					cylinder(d=7.5,h=10,center=true);
			}
		}

	}
}

module case() {
	difference() {
		baseshape(2,h=25);
		translate([0,0,2])baseshape(d=-1,h=25);
		translate([0,0,12])baseshape(d=0.5,h=3);
		translate([0,-30,12.5])rotate([90,0])cylinder(d=24,h=10,center=true);
	}
	difference() {
		mount(d=8,h=21);
		mount(h=22);
	}
	
} 

front($fn=80);
//translate([0,0,-12])case($fn=80);